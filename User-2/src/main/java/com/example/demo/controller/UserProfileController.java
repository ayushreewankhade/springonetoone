package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.UserProfileDto;
import com.example.demo.entity.User;
import com.example.demo.service.UserProfileService;

@RestController
public class UserProfileController {

	@Autowired
	private UserProfileService userProfileService;
	
	@PostMapping("/saveProfile")
	public ResponseEntity<?> addUserProfile( @RequestBody UserProfileDto userProfileDto, User user) {
		//User user= this.userProfileService.createUserProfile(userProfileDto, user)
			this.userProfileService.createUserProfile(userProfileDto, user);
			return new ResponseEntity<>(HttpStatus.OK);	
		
	}
	@GetMapping("/user-profile")
	public List<UserProfileDto> getAllUserProfile(){
		return userProfileService.getAllUserProfile();
	}
}
