package com.example.demo.dto;

import java.util.List;

import com.example.demo.entity.UserProfile;

public class UserDto {
	private int id;

	private String name;

	private String address;
	
	//private List<UserProfile> userProfile;

	public UserDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public UserDto(int id, String name, String address) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
	}

	/*public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}*/

	//public List<UserProfile> getUserProfile() {
	//	return userProfile;
	//}

	//public void setUserProfile(List<UserProfile> userProfile) {
	//his.userProfile = userProfile;
	//
	
	
	
}
