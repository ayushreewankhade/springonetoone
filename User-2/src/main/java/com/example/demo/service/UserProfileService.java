package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.UserProfileDto;
import com.example.demo.entity.User;
import com.example.demo.entity.UserProfile;

public interface UserProfileService {

	UserProfile createUserProfile(UserProfileDto userProfileDto, User user);

	List<UserProfileDto> getAllUserProfile();
	
	
}
