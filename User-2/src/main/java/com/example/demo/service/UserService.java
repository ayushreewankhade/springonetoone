package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.UserDto;
import com.example.demo.entity.User;

public interface UserService {

	User  creatUser(UserDto user);

	void updateUser(UserDto userDto,Integer id);

	UserDto getUserById(Integer userId);

	List<UserDto> getAllUser();

	public void deleteUser(Integer userId);
}
