package com.example.demo.serviceImpl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
//import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.UserDto;
import com.example.demo.dto.UserProfileDto;
import com.example.demo.entity.User;
import com.example.demo.entity.UserProfile;
import com.example.demo.repository.UserProfileRepo;
import com.example.demo.repository.UserRepo;
import com.example.demo.service.UserProfileService;

@Service
public class UserProfileServiceImpl implements UserProfileService {

	@Autowired
	private UserProfileRepo userProfileRepo;
	
	@Autowired
	private ModelMapper modelMapper;
	
	private UserProfileDto userProfileToDto(UserProfile userProfile)
	{
		UserProfileDto userProfileDto=this.modelMapper.map(userProfile,UserProfileDto.class);
		return userProfileDto;
	}
	
	@Override
	public UserProfile createUserProfile(UserProfileDto userProfileDto, User user) {
		UserProfile userProfile= new UserProfile();
		//userProfile.setId(userProfileDto.getId());
		userProfile.setUser(user);
		userProfile.setPhoneNumber(userProfileDto.getPhoneNumber());
		return this.userProfileRepo.save(userProfile);
	}

	@Override
	public List<UserProfileDto> getAllUserProfile() {
		List<UserProfile> userProfile= this.userProfileRepo.findAll();
		List<UserProfileDto> saveProfile= userProfile.stream().map(e-> this.userProfileToDto(e)).collect(Collectors.toList());
		return saveProfile; 
	}

	

}
